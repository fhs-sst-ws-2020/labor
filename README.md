# SST WS 2020: Lab Tutorials

This is the demo microservices project created for the SST lecture. It is illustrating how to create such an infrastructure and how to include it into a CI/CD workflow.  

## Architecture

![Architecture](https://gitlab.com/fhs-sst-ws-2020/labor/-/wikis/uploads/b27ba330223f32b3115bdae1fba526ab/architecture.png "Architecture")

## Submodules

* Add a module: `git submodule add <ssh git url>`
* For the first time checking out all submodules execute `git submodule update --init --recursive`
* After that pulling all sub modules works with: `git pull origin master --recurse-submodules`

## Docker Compose

Login to registry of gitlab:
 
* LOGIN: docker login registry.gitlab.com

Useful docker-compose commands:

* Pull images: `docker-compose pull`
* Run services in backgroun: `docker-compose up -d`
* Follow logs: `docker-compose logs -f`
* Get services status: `docker-compose ps`
* Scale service (3x task-app): `docker-compose scale task-app=3`

Build & Run services locally:

1) Build: `docker-compose -f docker-compose-local.yml build`
2) Run: `docker-compose -f docker-compose-local.yml up`

## Kubernetes

Created namespace:

```
kubectl create namespace task-app-dev
```

Apply all Deployments:

```
kubectl apply -f ./kubernetes
```

Access the API via the ELB DNS name:
```
curl http://aa5bd2155e90e4b04a29addecf2cd606-1279162324.eu-central-1.elb.amazonaws.com/api/v1/projects
```

Delete Deployment:
```
kubectl delete -f ./kubernetes
```
